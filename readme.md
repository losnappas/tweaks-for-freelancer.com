# Tweaks for Freelancer.com

https://addons.mozilla.org/en-US/firefox/addon/tweaks-for-freelancer-com/

Firefox extension for Freelancer.com.

* Add 'w=f' to urls to fix resistFingerprinting breaking everything.
* Add a button to fetch other peoples' bid messages, like if you were offline.
