'use strict'
 
const ckban = document.querySelector('.Card.CookieBanner.is-active')
if (ckban)
    ckban.remove()
const fban = document.querySelector('#fixed-banner')
if (fban)
    fban.remove()


const wrapper = document.createElement('div')
wrapper.className='span8 margin-t20'
const targetEl = '#projectBrief'
const run = () => {
    // remove old entries before fetching again.
	const oldOnes = document.querySelector('.tweaks-fetched-entries')
	if ( oldOnes ) {
		oldOnes.remove()
    } 
    fetch(location.href,
        {
            credentials: "omit"
        }
    )
      .then(res => res.text())
      .then(html => {
        const template = document.createElement('template')
        template.innerHTML = html
        const els = template.content.querySelectorAll('.FreelancerInfo-about')

		const innerWrapper = document.createElement( 'span' )
		innerWrapper.className = 'tweaks-fetched-entries'
		
        Array.prototype.forEach.call(els, e => {
          const p = document.createElement('p')
          const btn = document.createElement('button')
          btn.onclick = () => {
            let t = p.style.display === 'block' ? 'none' : 'block'
            p.style.display = t
            btn.innerText = btn.innerText === '+' ? '-' : '+'
          }
          btn.type = 'button'
          btn.innerHTML='+'
          p.innerText = e.dataset['descrFull']
          p.style.display = 'none'
          innerWrapper.append(btn)
          innerWrapper.append(p)
        })
        wrapper.append(innerWrapper)
      })
}
const fetchBtn = document.createElement('button')
fetchBtn.onclick = () => run()
fetchBtn.type = 'button'
fetchBtn.innerHTML = 'Fetch others\' bids'

wrapper.append(fetchBtn)
 
let cardbody = document.querySelector(targetEl)
if ( ! cardbody ) {
    cardbody = document.querySelector('.Card-body')
}
 
if (cardbody) {
    cardbody.append(wrapper)
}
