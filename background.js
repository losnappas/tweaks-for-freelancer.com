function addwf(request) {
	let url = new URL(request.url)
	if ( ! url.search.includes('w=f') ) {
		let wf = ''
		if ( url.search === "" ) {
			wf = '?w=f'
		} else {
			wf = '&w=f'
		}
		url.search += wf
		return { redirectUrl: url.href }
	}
}
browser.webRequest.onBeforeRequest.addListener(
	addwf,
	{ urls: ['*://*.freelancer.com/*'] },
	["blocking"]
)
